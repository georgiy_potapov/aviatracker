let Responser = function() {
	
	const KTStoKMH = 1.852;
	const FEETtoM = 3.281;	
	const DOMODEDOVO_LONGITUDE = 37.902451 * Math.PI / 180;
	const DOMODEDOVO_LATITUDE = 55.410307 * Math.PI / 180;
	
	let currentUrl = 'https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=55.41,54.41,36.90,37.90';
	
	this.setCurrentUrl = function( url ) {
		currentUrl = url;
	}
	
	this.getResponse = function() {
		
		let httpReq = new XMLHttpRequest();
		httpReq.open( "GET", currentUrl, true );
			
		httpReq.onload = function() {
			if ( httpReq.readyState == XMLHttpRequest.DONE && httpReq.status == 200 ) {
				let dataArray = parseResponse( JSON.parse( httpReq.responseText ) );
				generateTableView( dataArray );
			}
			else 
				document.getElementById("tst").innerHTML = "error - " + httpReq.statusText;	
		}
		
		httpReq.send();
	}
	
	let parseResponse = function( response ) {
		
		let dataArray = [];
		
		for( let key in response )
		{
			if( key == 'full_count' || key == 'version' )
				continue;
			
			let currentInfo = [];
			
			currentInfo.push( response[key][1] ); // широта в градусах
			currentInfo.push( response[key][2] ); // долгота в градусах
			currentInfo.push( (response[key][5] * KTStoKMH ).toFixed(2) ); // скорость км/ч
			currentInfo.push( response[key][3] ); // угол
			currentInfo.push( (response[key][4] / FEETtoM).toFixed(2) ); // высота полета
			currentInfo.push( response[key][11] ); // аэропорт отбытия
			currentInfo.push( response[key][12] ); // аэропорот прибытия
			currentInfo.push( response[key][13] ); // номер рейса
			
			dataArray.push( currentInfo );
		}	
		
		dataArray.sort( function(a, b) {
			let lat_rad_1 = a[0] * Math.PI / 180;
			let lon_rad_1 = a[1] * Math.PI / 180;
			let lat_rad_2 = b[0] * Math.PI / 180;
			let lon_rad_2 = b[1] * Math.PI / 180;
			
			let dist1 = Math.acos( Math.sin( lat_rad_1 ) * Math.sin( DOMODEDOVO_LATITUDE ) + Math.cos( lat_rad_1 ) * Math.cos( DOMODEDOVO_LATITUDE ) * Math.cos( lon_rad_1 - DOMODEDOVO_LONGITUDE ) ) * 6371;
			let dist2 = Math.acos( Math.sin( lat_rad_2 ) * Math.sin( DOMODEDOVO_LATITUDE ) + Math.cos( lat_rad_2 ) * Math.cos( DOMODEDOVO_LATITUDE ) * Math.cos( lon_rad_2 - DOMODEDOVO_LONGITUDE ) ) * 6371;
			
			return dist1 - dist2;
		});
		
		return dataArray;
	}
	
	let generateTableView = function( dataArray ) {
		
		let table = document.createElement( "table" );
		
		for( let i = 0; i < dataArray.length; ++i )
		{
			let newRow = table.insertRow( i );
			
			for( let j = 0; j < dataArray[i].length; ++j )
			{
				let newCell = newRow.insertCell( j );
				newCell.width = "200px";
				newCell.innerHTML = dataArray[i][j];
			}		
		}	
			
		if( document.getElementById("tst").firstChild )
			document.getElementById("tst").replaceChild( table, document.getElementById("tst").firstChild );
		else 
		document.getElementById("tst").appendChild( table );
	}	
};


